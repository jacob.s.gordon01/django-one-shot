from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from todos.models import TodoItem, TodoList

# Create your views here.

class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name", "date"]
    success_url = reverse_lazy("todos_list")


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name", "date"]
    success_url = reverse_lazy("todos_list")


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/new.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/update.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")